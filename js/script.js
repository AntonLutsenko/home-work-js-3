// 1. функции необходимы для упрощения кода и уменьшения + если она написана 1н раз, то на нее можно ссылаться в последующем коде и она будет работать.
// 2. на места аргументов передаются необходимые значения. благодаря этому функцию можно использовать с любыми названиями переменных или констант

const firstUserNumber = +prompt('Enter first number');
const secondUserNumber = +prompt('Enter second number');
const operator = prompt('Enter operand')

function calculator(a, b, c) {
    switch (c) {
        case '+' :
            return a + b;
        case '*' :
                return a * b;
        case '-' :
            return a - b;
        case '/' :
            return a / b;
    }
}

console.log(calculator(firstUserNumber, secondUserNumber, operator));